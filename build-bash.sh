#!/bin/bash
set -euo pipefail

yum install -y tar gcc perl

_BASH_VERSION="bash-4.4"

curl --silent --location "https://ftpmirror.gnu.org/bash/${_BASH_VERSION}.tar.gz" \
    | tar --extract --gzip --file - --directory /usr/src/

cd "/usr/src/${_BASH_VERSION}"
./configure --prefix=/opt/
make
make install

tar -zcf "${CI_PROJECT_DIR}/bash.tgz" -C /opt/ .
